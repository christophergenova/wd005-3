// Initialize a count variable;
let count = 0;

// 2. Manipulate the DOM and put the count value in h1 with id="count"
//
// 2.1 capture h1
// 
const countH1 = document.getElementById('count');
// this part will be done after refactoring the code(see notes after minusBtn)
function updateCountInH1(){
	countH1.textContent = count;
}

updateCountInH1();


// 2.2 modify the textContent of countH1;
// countH1.textContent = count;


// +
// 3 capture the + button.
// 3.1 form the div with id="btn-group", traverse to + button.
// 
const btnGroup = document.getElementById('btn-group');

const addBtn = btnGroup.lastElementChild;
// 4. add event listener
addBtn.addEventListener("click", function(){
	// we want to increase the count by 1.
	count++;

	// also we want to change the value of count h1;
	updateCountInH1();
});

// for the rest of the buttons, just repeat items #3-4

const minusBtn = btnGroup.firstElementChild;
minusBtn.addEventListener("click", function(){
	count--;
	updateCountInH1();
})

// we notice, we typed countH1.textContent = count 3x. and that violates the DRY principle. Since we realized it is a task, we can now rewrite it into a function.

// const resetBtn = btnGroup.firstElementChild.nextElementSibling;
const resetBtn = minusBtn.nextElementSibling;
resetBtn.addEventListener("click", function(){
	count = 0;
	updateCountInH1();
})

// Create auth.html
// auth.js