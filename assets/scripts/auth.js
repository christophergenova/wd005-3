// capture the value of registerEmnail and registerPassword

let registerEmail = "";
let registerPassword = "";

// to capture the values of registerEmail and registerPassword, we need to capture first the button.

// 
const registerBtn = document.getElementById('registerBtn');

registerBtn.addEventListener('click', function(){

	const registerEmailValue = document.getElementById('registerEmail').value;

	const registerPasswordValue = document.getElementById('registerPassword').value;

	// if password or email is blank validation
	if(registerEmailValue === "" || registerPasswordValue === ""){
		alert("Email or Password cannot be blank.");
		return;
	}
	// validation if password length is less than 8
	if(registerPasswordValue.length < 8) {
		alert("Password length must be 8 or longer");
		return;
	}
	// 1. save the values in registerEmail and registerPassword
	registerEmail = registerEmailValue;
	registerPassword = registerPasswordValue;

	// 2. hide the register div
	// 2.1 capture the register div
	const registerScreen = document.getElementById('registerScreen');

	// 2.2 access the classlist and remove d-flex
	registerScreen.classList.remove('d-flex');
	// 2.3 access the classlist and add d-none
	registerScreen.classList.add('d-none');
	// 3. show the login div
	// 3.1 capture loginscreen
	const loginScreen = registerScreen.nextElementSibling;

	// 3.2 access classlist and remove d-none
	loginScreen.classList.remove('d-none')
	// 3.3 access class list and add d-flex
	loginScreen.classList.add('d-flex');

})

// Activity
// 1. capture the loginemail and loginpassword
const loginBtn = document.getElementById('loginBtn');

// 2. attach an event listener to the login button
loginBtn.addEventListener('click', function(){

	const loginEmailValue = document.getElementById('loginEmail').value;
	const loginPasswordValue = document.getElementById('loginPassword').value;

	// 2.1 add validation if email or password is empty
	if(loginEmailValue === "" || loginPasswordValue === ""){
		alert("Email or Password cannot be blank.");
		return;
	}

	loginEmail = loginEmailValue;
	loginPassword = loginPasswordValue;

	const loginScreen = document.getElementById('loginScreen');

	loginScreen.classList.remove('d-flex');

	loginScreen.classList.add('d-none');

	const successScreen = loginScreen.nextElementSibling;

	successScreen.classList.remove('d-none')
	// 3.3 access class list and add d-flex
	successScreen.classList.add('d-flex');



})

// 2.2 compare the loginEmail and loginPassword and show the correct screen (fail or success)